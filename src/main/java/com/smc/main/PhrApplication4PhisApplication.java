package com.smc.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhrApplication4PhisApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhrApplication4PhisApplication.class, args);
	}

}
