package com.smc.main.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.smc.main.model.Patient;

@Controller
public class WebController {
	
	@GetMapping("/index")
	public String index(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        Patient patient = new Patient();
        patient.setName("Test Name");
		
		model.addAttribute("user", patient);
        return "index";
    }
	
	@GetMapping("/launch")
	public String launch(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "launch";
    }
	
	@GetMapping("/dashboard")
	public String dashboard(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "dashboard";
    }

}