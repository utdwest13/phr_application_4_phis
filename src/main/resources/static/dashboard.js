/* globals Chart:false, feather:false */

var state = getUrlParameter("state"); // session key
var code = getUrlParameter("code"); // authorization code

// load the app parameters stored in the session
//var params = JSON.parse(sessionStorage[state]); // load app session
//var tokenUri = params.tokenUri;
//var clientId = params.clientId;
//var secret = params.secret;
//var serviceUri = params.serviceUri;
//var redirectUri = params.redirectUri;

var accessToken = null;
var patientId = null;

 var tokenUri = "http://localhost:9080/oauth/token";
 var clientId = "foo";
 var secret = null;
 var serviceUri = "http://localhost:9080/fhir";
 var redirectUri = "http://localhost:8091/dashboard";

// Prep the token exchange call parameters
var data = {
	code : code,
	grant_type : 'authorization_code',
	redirect_uri : redirectUri
};

if (!secret) {
	data['client_id'] = clientId;
}
options = {
	url : tokenUri,
	type : 'POST',
	data : data
};
// alert("token URL: " + tokenUri);
if (secret) {
	options['headers'] = {
		'Authorization' : 'Basic ' + btoa(clientId + ':' + secret)
	};
}

// obtain authorization token from the authorization service using
// the authorization code
$.ajax(options).done(
		function(res) {
			// should get back the access token and the patient ID
			// var accessToken = res.access_token;
			// alert("Access Token: " + accessToken);
			// var patientId = res.patient;

			accessToken = res.access_token;
			patientId = res.patient;

			// and now we can use these to construct standard FHIR
			// REST calls to obtain patient resources with the
			// SMART on FHIR-specific authorization header...
			// Let's, for example, grab the patient resource and
			// print the patient name on the screen
			var url = serviceUri + "/Patient/" + patientId;
			// var url = serviceUri + "/Patient/352";
			// alert("Get fhir URL: " + url);
			$.ajax({
				url : url,
				type : "GET",
				dataType : "json",
				headers : {
					"Authorization" : "Bearer " + accessToken
				},
			}).done(
					function(pt) {
						// alert("Output: " + JSON.stringify(pt));
						console.log(pt);
						// pt = JSON.Parse(pt);

						var name = pt.name[0].family + " "
								+ pt.name[0].given.join(" ");

						// alert(name);
						// var name = pt.name[0].given.join(" ") + " "
						// + pt.name[0].family;
						// document.body.innerHTML += "<h3>Patient: " + name
						// +
						// "</h3>";
						// $('h3').html("<h3>Patient: " + name + "</h3>");
						$('h2').html("성 명: " + name);
						$('thead').html(
								"<tr>" + "<th>생년</th>" + "<th>성별</th>"
										+ "<th>연락처</th>" + "<th>주소</th>"
										+ "</tr>");
						$('tbody').html("");
						$('tbody').append(
								"<tr>" + "<td>" + nvl(pt.birthDate, "정보 없음")
										+ "</td>" + "<td>"
										+ nvl(pt.gender, "정보 없음") + "</td>"
										+ "<td>" + nvl(pt.gender, "정보 없음")
										+ "</td>" + "<td>"
										+ nvl(pt.address, "정보 없음") + "</td>"
										+ "</tr>");
						$('.table-responsive').show();
						/*
						 * jQuery 2 error -> 3 fail }).error(
						 */
					}).fail(
					function(request, status, error) {
						alert("code:" + request.status + "\n" + "message:"
								+ request.responseText + "\n" + "error:"
								+ error);
						console.log("code:" + request.status + "\n"
								+ "message:" + request.responseText + "\n"
								+ "error:" + error);

					});
		});

(function() {

	$('.table-responsive').hide();

	'use strict'

	feather.replace()

	// Graphs
	var ctx = document.getElementById('myChart')
	// eslint-disable-next-line no-unused-vars
	var myChart = new Chart(ctx,
			{
				type : 'line',
				data : {
					labels : [ '1', '25', '3', '4', '5', '6', '7', '8' ],
					datasets : [ {
						data : [ 15339, 21345, 18483, 12034, 13455, 12345,
								23445, 23456 ],
						lineTension : 0,
						backgroundColor : 'transparent',
						borderColor : '#007bff',
						borderWidth : 4,
						pointBackgroundColor : '#007bff'
					} ]
				},
				options : {
					scales : {
						yAxes : [ {
							ticks : {
								beginAtZero : false
							}
						} ]
					},
					legend : {
						display : false
					}
				}
			})
}())

function nvl(str, defaultStr) {

	if (typeof str == "undefined" || str == null || str == "")
		str = defaultStr;

	return str;
}

// Convenience function for parsing of URL parameters
// based on
// http://www.jquerybyexample.net/2012/06/get-url-parameters-using-jquery.html
function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			var res = sParameterName[1].replace(/\+/g, '%20');
			return decodeURIComponent(res);
		}
	}
}

// Convert FHIR Date to Order Date
function convertFhirDatetoMedReqDate(date) {
	date = date.substring(0, 10);
	return date;
}

// Check including the valueQuantity
function representingValueUnitForm(entry) {

	var str = null;
	var valueQuantity = entry.resource.valueQuantity;
	if (typeof valueQuantity == "undefined" || valueQuantity == null
			|| valueQuantity == "") {

		if (typeof entry.resource.valueString == "undefined"
				|| entry.resource.valueString == null) {
			str = "";
		} else {
			str = entry.resource.valueString;
		}

	} else {
		if (typeof valueQuantity.unit == "undefined"
				|| valueQuantity.unit == null || valueQuantity.unit == "") {
			str = valueQuantity.value;
		} else {
			str = valueQuantity.value + " " + valueQuantity.unit;
		}
	}

	return str

}

/* Get MedicationRequest */
function getMedicationRequest() {

	var url = serviceUri + "/MedicationRequest?subject=Patient/" + patientId
			+ "&authoredon=2019-09-26";

	$
			.ajax({
				url : url,
				type : "GET",
				dataType : "json",
				headers : {
					"Authorization" : "Bearer " + accessToken
				},
			})
			.done(
					function(bundle) {
						console.log(bundle);

						$('thead').html(
								"<tr>" + "<th>#</th>" + "<th>처방일</th>"
										+ "<th>약물명</th>" + "<th>1일투여횟수</th>"
										+ "<th>용법</th>" + "</tr>");

						$('tbody').html("");
						$
								.each(
										bundle.entry,
										function(index, entry) {
											console.log(index + ": "
													+ entry.resource.id);
											$('tbody')
													.append(
															"<tr>"
																	+ "<td>"
																	+ (index + 1)
																	+ "</td>"
																	+ "<td>"
																	+ convertFhirDatetoMedReqDate(entry.resource.authoredOn)
																	+ "</td>"
																	+ "<td>"
																	+ entry.resource.medicationCodeableConcept.coding[0].display
																	+ "</td>"
																	+ "<td>"
																	+ entry.resource.dosageInstruction[0].timing.repeat.frequency
																	+ "</td>"
																	+ "<td>"
																	+ entry.resource.dosageInstruction[0].text
																	+ "</td>"
																	+ "</tr>");
										});
					}).fail(
					function(request, status, error) {
						console.log("code:" + request.status + "\n"
								+ "message:" + request.responseText + "\n"
								+ "error:" + error);
					});
}

/* Get Observation */
function getObservation() {

	var url = serviceUri + "/Observation?subject=Patient/" + patientId;

	// alert("Get fhir URL: " + url);
	$.ajax({
		url : url,
		type : "GET",
		dataType : "json",
		headers : {
			"Authorization" : "Bearer " + accessToken
		},
	}).done(
			function(bundle) {
				console.log(bundle);

				$('thead').html(
						"<tr>" + "<th>#</th>" + "<th>검사날짜</th>"
								+ "<th>검사코드</th>" + "<th>결과값</th>" + "</tr>");

				$('tbody').html("");
				$.each(bundle.entry, function(index, entry) {
					console.log(index + ": " + entry.resource.id);
					$('tbody').append(
							"<tr>"
									+ "<td>"
									+ (index + 1)
									+ "</td>"
									+ "<td>"
									+ entry.resource.effectiveDateTime.replace(
											"T", " ").substring(0, 19)
									+ "</td>" + "<td>"
									+ entry.resource.code.text + "</td>"
									+ "<td>" + representingValueUnitForm(entry)
									+ "</td>" + "</tr>");
				});

				// Change My Chart
				var ctx = document.getElementById('myChart')
				var myChart = new Chart(ctx, {
					type : 'line',
					data : {
						labels : [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
								'Sun' ],
						datasets : [ {
							data : [ {
								x : 'Tue',
								y : 15339
							},

							21345,

							12034, 13455, 12345, 23445 ],
							lineTension : 0,
							backgroundColor : 'transparent',
							borderColor : '#007bff',
							borderWidth : 4,
							pointBackgroundColor : '#007bff'
						} ]
					},
					options : {
						scales : {
							yAxes : [ {
								ticks : {
									beginAtZero : false
								}
							} ]
						},
						legend : {
							display : false
						}
					}
				});

				/*
				 * jQuery 2 error -> 3 fail }).error(
				 */
			}).fail(
			function(request, status, error) {
				console.log("code:" + request.status + "\n" + "message:"
						+ request.responseText + "\n" + "error:" + error);

			});

}
